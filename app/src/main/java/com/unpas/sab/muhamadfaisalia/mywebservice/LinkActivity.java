package com.unpas.sab.muhamadfaisalia.mywebservice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LinkActivity extends AppCompatActivity {
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link);
        webView = new WebView(this);
        webView = (WebView) findViewById(R.id.webview);
        Intent i = getIntent();
        Bundle b= i.getExtras();
        if (b != null){
            String url = (String) b.get("url");
            webView.loadUrl(url);
            webView.setWebViewClient(new WebViewClient());
        }
    }
}
