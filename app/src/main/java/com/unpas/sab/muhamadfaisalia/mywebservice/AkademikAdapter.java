package com.unpas.sab.muhamadfaisalia.mywebservice;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

public class AkademikAdapter extends RecyclerView.Adapter<AkademikAdapter.ViewHolder> {

    private ArrayList<HashMap<String, String>> postList;
    private MainActivity activity;
    private Context context;

    public AkademikAdapter(ArrayList<HashMap<String, String>> postList, MainActivity activity) {
        this.postList = postList;
        this.activity = activity;
    }

    public AkademikAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_akademik, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final HashMap<String, String> post = postList.get(position);
        Glide.with(activity).load(post.get("img_url")).into(holder.imgLogo);
        holder.textSingkatan.setText(post.get("singkatan"));
        holder.textNama.setText(post.get("nama"));
        holder.textUrl.setText(post.get("url"));
        holder.textUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = holder.textUrl.getText().toString();
                Intent i = new Intent(v.getContext(), LinkActivity.class);
                i.putExtra("url", url);
                v.getContext().startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgLogo;
        TextView textSingkatan;
        TextView textNama;
        TextView textUrl;

        public ViewHolder(View v){
            super(v);
            imgLogo = (ImageView) v.findViewById(R.id.imgLogo);
            textSingkatan = (TextView) v.findViewById(R.id.textSingkatan);
            textNama = (TextView) v.findViewById(R.id.textNama);
            textUrl = (TextView) v.findViewById(R.id.textUrl);
        }
    }
}
